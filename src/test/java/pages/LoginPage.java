package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class LoginPage {

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "pass")
    private WebElement passwordField;

    @FindBy(id = "u_0_b")
    private WebElement loginBtn;

    @FindBy(id = "pageFooter")
    private WebElement footer;

    @FindBy(linkText = "Log In")
    private WebElement login;

    @FindBy(linkText = "Sign Up")
    private WebElement signup;

    public void validateEmailPassField() {
        Assert.assertTrue(emailField.isDisplayed());
        Assert.assertTrue(passwordField.isDisplayed());
    }

    public void fillEmailAndPassField(String email, String pass) {
        emailField.sendKeys(email);
        passwordField.sendKeys(pass);
    }

    public void clickOnLoginButtn() {
        loginBtn.click();
    }

    public void validateFooter() {
        Assert.assertTrue(footer.isDisplayed());
    }

    public void validateSignUpAndLogin() {
        Assert.assertTrue(login.isDisplayed());
        Assert.assertTrue(signup.isDisplayed());
    }
}
