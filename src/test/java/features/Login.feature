Feature: Login Functionality Validation

  @Regression
  Scenario Outline: user being able to login to facebook using valid credentials
    Given user opens the browser and navigate to facebook login page
    When user can see the username and password field
    Then user provides valid <username> and valid <password> in the required field
    And user clicks on login button
    And user can see the homepage
    And user closes the browser

    Examples:
      | username        | password       |
      | test@gmail.com  | myTestPass233  |
      | test2@gmail.com | myTest2Pass233 |

  @Smoke
  Scenario:
    Given user opens the browser and navigate to facebook login page
    When user sees the footer of the page
    Then user validates signup and login
    And user closes the browser
