package stepdefinations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.PageFactory;
import pages.LoginPage;

public class LoginSteps {

    @Given("^user opens the browser and navigate to facebook login page$")
    public void user_opens_the_browser_and_navigate_to_facebook_login_page() {
        DriverInit.getDriver();
        DriverInit.driver.get("https://www.facebook.com");
    }

    @When("^user can see the username and password field$")
    public void user_can_see_the_username_and_password_field() {
        LoginPage loginPage = PageFactory.initElements(DriverInit.driver, LoginPage.class);
        loginPage.validateEmailPassField();
    }

  /*  @Then("^user provides valid username and valid password in the required field$")
    public void user_provides_valid_username_and_valid_password_in_the_required_field() {
        LoginPage loginPage = PageFactory.initElements(DriverInit.driver, LoginPage.class);
        loginPage.fillEmailAndPassField("Test@gmail.com", "245245");
    }*/

    @Then("^user provides valid (.*) and valid (.*) in the required field$")
    public void user_provides_valid_username_and_valid_password_in_the_required_fields(String user, String pass) {
        LoginPage loginPage = PageFactory.initElements(DriverInit.driver, LoginPage.class);
        loginPage.fillEmailAndPassField(user, pass);
    }


    @Then("^user clicks on login button$")
    public void user_clicks_on_login_button() {
        LoginPage loginPage = PageFactory.initElements(DriverInit.driver, LoginPage.class);
        loginPage.clickOnLoginButtn();
    }

    @Then("^user can see the homepage$")
    public void user_can_see_the_homepage() {
        // validation of user being logged in -> to do
    }

    @Then("^user closes the browser$")
    public void user_closes_the_browser() {
        DriverInit.driver.close();
    }

}
