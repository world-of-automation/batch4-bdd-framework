package stepdefinations;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.PageFactory;
import pages.LoginPage;

public class LandingPageSteps {

    @When("^user sees the footer of the page$")
    public void user_sees_the_footer_of_the_page() {
        LoginPage loginPage = PageFactory.initElements(DriverInit.driver, LoginPage.class);
        loginPage.validateFooter();
    }

    @Then("^user validates signup and login$")
    public void user_validates_signup_and_login() {
        LoginPage loginPage = PageFactory.initElements(DriverInit.driver, LoginPage.class);
        loginPage.validateSignUpAndLogin();
    }
}
